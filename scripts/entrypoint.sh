#!/bin/sh

DIR=$(dirname "${0}")

. "${DIR}/spark-entrypoint-helpers.sh"

# For Zeppelin properties, see
# https://zeppelin.apache.org/docs/latest/install/configuration.html
# https://zeppelin.apache.org/docs/latest/interpreter/spark.html

# notebook dir

if [ -n "${PROP_ZEPPELIN_zeppelin_notebook_dir}" ]; then
	export ZEPPELIN_NOTEBOOK_DIR="${PROP_ZEPPELIN_zeppelin_notebook_dir}"
elif [ -z "${ZEPPELIN_NOTEBOOK_DIR}" ]; then
	export ZEPPELIN_NOTEBOOK_DIR="${ZEPPELIN_HOME}/notebook"
	echo "Variable 'ZEPPELIN_NOTEBOOK_DIR' should be set and mounted as a volume (otherwise, the default '${ZEPPELIN_NOTEBOOK_DIR}' will be utilized)!" >&2
fi

mkdir -vp "${ZEPPELIN_NOTEBOOK_DIR}"
chmod -v 700 "${ZEPPELIN_NOTEBOOK_DIR}"
chown -R zeppelin:zeppelin "${ZEPPELIN_NOTEBOOK_DIR}"

export PROP_ZEPPELIN_zeppelin_notebook_dir="${ZEPPELIN_NOTEBOOK_DIR}"
echo "export ZEPPELIN_NOTEBOOK_DIR=${ZEPPELIN_NOTEBOOK_DIR}" >> "${ZEPPELIN_ENV_CONF}"

# WebUI

if [ -n "${PROP_ZEPPELIN_zeppelin_server_port}" ]; then
	export ZEPPELIN_PORT="${PROP_ZEPPELIN_zeppelin_server_port}"
elif [ -z "${ZEPPELIN_PORT}" ]; then
	export ZEPPELIN_PORT="8080"
	echo "Variable 'ZEPPELIN_PORT' should be set (otherwise, the default '${ZEPPELIN_PORT}' will be utilized)!" >&2
fi

export PROP_ZEPPELIN_zeppelin_server_port="${ZEPPELIN_PORT}"
echo "export ZEPPELIN_PORT=${ZEPPELIN_PORT}" >> "${ZEPPELIN_ENV_CONF}"

# Spark

if [ -n "${PROP_ZEPPELIN_master}" ]; then
	export MASTER="${PROP_ZEPPELIN_master}"
elif [ -z "${MASTER}" ]; then
	export MASTER="local[*]"
	echo "Variable 'MASTER' should be set correctly to a Spark master URL (otherwise, the default '${MASTER}' will be utilised)!" >&2
fi

export PROP_ZEPPELIN_master="${MASTER}"
echo "export MASTER=${MASTER}" >> "${ZEPPELIN_ENV_CONF}"

set_histui_port
set_log_dir
expand_spark_jars_dirs

# YARN

if [ -z "${PROP_YARN_yarn_resourcemanager_hostname}" -a -n "${RESOURCE_MANAGER}" ]; then
	export PROP_YARN_yarn_resourcemanager_hostname="${RESOURCE_MANAGER}"
fi

# HDFS

if [ -z "${PROP_ZEPPELIN_hdfs_url}" -a -n "${HDFS_URL}" ]; then
	export PROP_ZEPPELIN_hdfs_url="${HDFS_URL}"
fi

# Livy

if [ -z "${PROP_ZEPPELIN_zeppelin_livy_url}" -a -n "${LIVY_URL}" ]; then
	export PROP_ZEPPELIN_zeppelin_livy_url="${LIVY_URL}"
fi

#

. "${DIR}/hadoop-set-props.sh"
. "${DIR}/spark-set-props.sh"
. "${DIR}/zeppelin-set-props.sh"

exec su zeppelin -c "exec ${ZEPPELIN_HOME}/bin/zeppelin.sh ${@}"
